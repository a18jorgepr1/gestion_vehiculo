package vehiculo;

import vehiculo.util.Util;
import java.time.LocalDate;
import java.util.Scanner;

/**
 * Aplicación Java que permita gestionar la información de un vehículo. 
 * Mediante un menú que aparecerá en pantalla se podrán realizar determinadas operaciones:<p>
 * 
 * 1.  Nuevo vehículo.<br>
 * 2.  Ver matrícula.<br>
 * 3.  Ver número de kilómetros.<br>
 * 4.  Actualizar kilómetros.<br>
 * 5.  Ver años de antigüedad.<br>
 * 6.  Mostrar propietario.<br>
 * 7.  Mostrar descripción.<br>
 * 8.  Mostrar precio.<br>
 * 9.  Salir.<br>
 * 
 * @author jorgePicon
 * @version 0.0.1-SNAPSHOT
 */
public class ConsoleApp {

    public final static String ADVERTENCIA = "Introduzca primero un nuevo vehículo";
    public final static char EURO = '\u20ac';
    public static LocalDate f;
    public static String _sys;

    //@SuppressWarnings("resource") // Evita la advertencia de entrada(Scanner) no cerrado
    static Scanner entrada = new Scanner(System.in);
    
    /**
     * Método main de la aplicación Gestion_Vehiculo:
     * 
     * @param args
     * @throws InterruptedException
     */
    public static void main(String[] args) throws InterruptedException {
        
        //Comprobación del sistema operativo en el que estamos:
        Util.getSystem();

        //Creación de un vehículo sin parametros para comprobar si el usuario ha introducido un nuevo vehículo:
        Vehiculo v = null;
        String pulsarTecla;
        do {
            pulsarTecla = imprimirMenu();

            switch (pulsarTecla) {
                case "1":
                    v = nuevoVehiculo();
                    break;
                case "2":
                    System.out.println("Matrícula del vehículo: ");
                    System.out.println((v == null) ? ADVERTENCIA : v.getMatricula());
                    mantenerPantalla();
                    break;
                case "3":
                    System.out.println("Número de kilómetros del vehículo: ");
                    System.out.println((v == null) ? ADVERTENCIA : v.getNumKilometros() + " km.");
                    mantenerPantalla();
                    break;
                case "4":
                    int km = 0;
                    if (v != null) {
                        System.out.println("Actualizando kilometraje... \n- Introduzca los kilómetros actuales del vehículo: ");
                        km = Util.toInt(entrada.nextLine());
                        if (km <= v.getNumKilometros()) {
                            System.out.printf("El número de kilómetros introducidos debe ser superior a %d!", v.getNumKilometros());
                            mantenerPantalla();
                            break;
                        }
                    } else {
                        System.out.print(ADVERTENCIA);
                        mantenerPantalla();
                        break;
                    }
                    v.setNumKilometros(km);
                    System.out.println("Kilómetros actualizados!");
                    mantenerPantalla();
                    break;
                case "5":
                    System.out.println("Antiguedad del vehículo: ");
                    System.out.println((v == null) ? ADVERTENCIA : (LocalDate.now().getYear() - v.getFechaMatriculacion().getYear()) + " años.");
                    mantenerPantalla();
                    break;
                case "6":
                    System.out.println("Propietario del vehículo: ");
                    System.out.println((v == null) ? ADVERTENCIA : v.getPropietario() + " con DNI/NIF " + v.getDNI());
                    mantenerPantalla();
                    break;
                case "7":
                    System.out.println("Descripción del vehículo: ");
                    System.out.println((v == null) ? ADVERTENCIA : v.getDescripcion());
                    mantenerPantalla();
                    break;
                case "8":
                    System.out.println("Precio del vehículo: ");
                    System.out.println((v == null) ? ADVERTENCIA : v.getPrecio() + " " + EURO);
                    mantenerPantalla();
                    break;
                case "9":
                    System.out.println("Saliendo del programa... Hasta pronto!\n");
                    break;
                default:
                    System.out.print("La opción '" + pulsarTecla + "' no es válida, vuelva a intentarlo");
                    mantenerPantalla();
            }
        } while (!pulsarTecla.equalsIgnoreCase("9"));
    }

    /**
     * Método que imprime el menú principal 
     * y devuelve el número de opción seleccionada mediante la entrada standard.
     * 
     * @return String
     */
    public static String imprimirMenu() {
        //Llamamos al método que limpia la pantalla del terminal:
        Util.cls();

        System.out.println("************************************************");
        System.out.printf("%-15s %s %15s%n", "*", "GESTION VEHICULO", "*");
        System.out.println("************************************************");
        System.out.println(" Introduzca el número de opción deseada:");
        System.out.println(" 1. Nuevo vehículo.");
        System.out.println(" 2. Ver matrícula.");
        System.out.println(" 3. Ver número de kilómetros.");
        System.out.println(" 4. Actualizar kilómetros.");
        System.out.println(" 5. Ver años de antigüedad.");
        System.out.println(" 6. Mostrar propietario.");
        System.out.println(" 7. Mostrar descripción.");
        System.out.println(" 8. Mostrar precio.");
        System.out.println(" 9. Salir.\n");

        System.out.print("Opcion: ");
        String opcion = numeroOpcionSeleccionada();
        return opcion;
    }

    /**
     * Método que recoge el valor de la tecla pulsada por el usuario.
     * 
     * @return String
     */
    public static String numeroOpcionSeleccionada() {
        String numOpcion = entrada.nextLine();
        return numOpcion;
    }

    /**
     * Método que crea un nuevo vehículo. Pide al usuario que introduzca valores y accede a la clase Util para validarlos, 
     * si éstos son correctos se instancia un nuevo vehículo.
     * 
     * @return devuelve un nuevo objeto de tipo Vehículo
     * @see Vehiculo
     */
    public static Vehiculo nuevoVehiculo() {
        System.out.println("1. Creando un nuevo vehículo...");

        System.out.print("\n- Introduzca la marca del vehículo: ");
        String marca = entrada.nextLine();

        System.out.print("\n- Introduzca la matrícula del vehículo: ");
        String matricula = entrada.nextLine();
        
        System.out.print("\n- Introduzca la fecha de matriculación del vehículo: ");
        LocalDate f = Util.fechaMatriculacion(), fechaMatriculacion = null;
        if (Util.validaFechaMatriculacion(f)){
            fechaMatriculacion = f;
        }else{
            return null;
        }

        System.out.println("\n- Introduzca los kilómetros del vehículo: ");
        int km = Util.toInt(entrada.nextLine());
        if (!Util.validaKilometros(km)){
            System.out.println("El número de kilómetros introducidos debe ser superior a 0");
           return null;
        }
        int numKilometros = km;

        System.out.print("\n- Introduzca la descripción del vehículo: ");
        String descripcion = entrada.nextLine();

        System.out.print("\n- Introduzca el precio del vehículo: ");
        double precio = Util.toDouble(entrada.nextLine());

        System.out.print("\n- Introduzca el nombre del propietario del vehículo: ");
        String nomPropietario = entrada.nextLine();

        System.out.print("\n- Introduzca el dni del propietario del vehículo: ");
        String dni, nif = entrada.nextLine();
        if (Util.isNifNumValid(nif)) {
            System.out.println("DNI validado!");
            dni = nif;
        }else{ 
            System.out.println("El DNI no se ha podido validar!");
            mantenerPantalla();
            return null;
        }

        System.out.println("¡Se ha creado un nuevo vehículo!");
        mantenerPantalla();
        return new Vehiculo(marca, matricula, numKilometros, fechaMatriculacion, descripcion, precio, nomPropietario, dni);
    }
    /**
     * Método que mantiene la ultima impresión en pantalla hasta que el usuario presione enter.
     * 
     * @return void
     */
    public static void mantenerPantalla() {
        System.out.print("\nPresiona enter para continuar...");
        try {
            entrada.nextLine();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
