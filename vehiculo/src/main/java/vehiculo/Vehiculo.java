package vehiculo;

import java.time.LocalDate;

/**
 * Clase que contiene los atributos y los métodos necesarios para instanciar un nuevo objeto de tipo Vehiculo
 * 
 * @author jorgePicon
 * @version 0.0.1-SNAPSHOT
 */
public class Vehiculo {

    //Atributos de Vehiculo:
    private String marca; // Recoge el nombre del fabricante de un nuevo vehículo.
    private String matricula; // Recoge la matrícula del nuevo vehículo, acepta distintos formatos.
    private int numKilometros; // Recoge los kilómetros del nuevo vehiculo.
    private LocalDate fechaMatriculacion; // Recoge la fecha de matriculación del vehículo como un objeto LocalDate
    private String descripcion; // Recoge la descripción del vehículo.
    private double precio; // Recoge el precio del vehículo.
    private String nomPropietario; // Recoge el nombre del propietario del vehículo.
    private String dni; // Recoge el dni o nif del propietario del vehículo.

    //Constructor:
    /**
     * Método constructor con parámetros de un nuevo objeto de la clase Vehículo 
     *
     * @param marca
     * @param matricula
     * @param numKilometros
     * @param fechaMatriculacion
     * @param descripcion
     * @param precio
     * @param nomPropietario
     * @param dni
     * 
     * @see ConsoleApp#nuevoVehiculo()
     */
    public Vehiculo(String marca, String matricula, int numKilometros, LocalDate fechaMatriculacion,
            String descripcion, double precio, String nomPropietario, String dni) {
        this.marca = marca;
        this.matricula = matricula;
        this.numKilometros = numKilometros;
        this.fechaMatriculacion = fechaMatriculacion;
        this.descripcion = descripcion;
        this.precio = precio;
        this.nomPropietario = nomPropietario;
        this.dni = dni;
    }

    /*
     * Métodos getters y setters para obtención y modificación de parámetros del objeto Vehiculo
     * 
     */ 
    public String getMarca() { return marca; }
    public String getMatricula() { return matricula; }
    public int getNumKilometros() { return numKilometros; }
    //Metodo que actualiza los kilómetros del vehículo.
    public void setNumKilometros(int numKilometros) { this.numKilometros = numKilometros; }
    public LocalDate getFechaMatriculacion() { return fechaMatriculacion; }
    public void setFechaMatriculacion(LocalDate fecha) { fechaMatriculacion = fecha;}
    public String getPropietario() { return nomPropietario; }
    public String getDescripcion() { return descripcion; }
    public double getPrecio() { return precio; }
    public String getNomPropietario() { return nomPropietario; }
    public String getDNI() { return dni; }
}