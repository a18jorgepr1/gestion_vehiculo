package vehiculo.util;

import java.time.LocalDate;
import java.util.Scanner;
import vehiculo.ConsoleApp;

/**
 * Clase que contiene distintos métodos de validación de entrada de datos.
 * 
 * @author jorgePicon
 * @version 0.0.1-SNAPSHOT
 */
public class Util {

    public final static String SYS_NIX = "SYS_NIX";
    public final static String SYS_WIN = "SYS_WIN";
    
    //@SuppressWarnings("resource") // Evita la advertencia de entrada(Scanner) no cerrado
    static Scanner entrada = new Scanner(System.in);

    /**
     * Método que a partir de la entrada standard construye un objeto LocalDate
     * Pide el año, el mes y el dia
     * 
     * @return LocalDate
     */
    public static LocalDate fechaMatriculacion() {
        LocalDate fecha;
        int year, month, dayOfMonth;
        System.out.print("\nAño (4 digitos): ");
        year = entrada.nextInt();
        System.out.print("Mes (1 - 12): ");
        month = entrada.nextInt();
        System.out.print("Día (1 - 31): ");
        dayOfMonth = entrada.nextInt();
        fecha= LocalDate.of(year, month, dayOfMonth);
        return fecha;
    }

    /**
     * Método que valida si la fecha de matriculación de un nuevo vehículo es anterior a la fecha actual.
     * 
     * @param fecha
     * @return boolean
     */
    public static boolean validaFechaMatriculacion(LocalDate fecha) {
        boolean validado = false;

        if (fecha.isBefore(LocalDate.now())) {
            System.out.println("¡Fecha vadidada!");
            validado = true;
        }else{
            System.out.println("¡La fecha introducida debe ser anterior a la fecha actual!");
            ConsoleApp.mantenerPantalla();
        }
        return validado;
    }

    /**
     * Método que valida que el valor del parámetro que se le pasa sea mayor que 0, de ser así devuelve true.
     * 
     * @param k
     * @return boolean
     */
    public static boolean validaKilometros(int k) { return (k>=0 ? true : false); }

    /**
     * Método que devuelve un entero a partir de un String de un número decimal puntuado 
     * indistintamente con un punto o una coma.
     * 
     * @param i
     * @return int
     */
    public static int toInt(String i) {
        if (i.contains(",") || (i.contains("."))) {
            i = i.replace(",", ".");
            i = i.substring(0, i.indexOf("."));
        }
        return Integer.parseInt(i);
    }

    /**
     * Método que devuelve un double a partir de un String de un número decimal puntuado 
     * indistintamente con un punto o una coma.
     * 
     * @param i
     * @return int
     */
    public static Double toDouble(String i) {
        if (i.contains(",")) {
            i = i.replace(",", ".");
        }
        return Double.parseDouble(i);
    }

    /**
     * Método que determina si un dni o nif es válido.
     * 
     * @param nif
     * @return boolean
     */
    public static boolean isNifNumValid(String nif){
        //Si el largo del NIF es diferente a 9, acaba el método.
        if (nif.length()!=9){
            return false;
        }

        String secuenciaLetrasNIF = "TRWAGMYFPDXBNJZSQVHLCKE"; 
        nif = nif.toUpperCase();

        //Posición inicial: 0 (primero en la cadena de texto).
        //Longitud: cadena de texto menos última posición. Así obtenemos solo el número.
        String numeroNIF = nif.substring(0, nif.length()-1);

        //Si es un NIE reemplazamos letra inicial por su valor numérico.
        numeroNIF = numeroNIF.replace("X", "0").replace("Y", "1").replace("Z", "2");

        //Obtenemos la letra con un char que nos servirá también para el índice de las secuenciaLetrasNIF
        char letraNIF = nif.charAt(8);
        int i = Integer.parseInt(numeroNIF) % 23;
        return letraNIF == secuenciaLetrasNIF.charAt(i);
    }

    /** Limpia el terminal. */
    public static void cls() {
        switch (ConsoleApp._sys) {
            case SYS_NIX:
                System.out.print("\033[H\033[2J");
                break;
            case SYS_WIN:
                try {
                    // chapucilla windows
                    new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
        }
    }

    /** Identifica la plataforma. */
    public static void getSystem() {
        ConsoleApp._sys = SYS_NIX; // valor por defecto (linux, Mac)

        String os = System.getProperty("os.name").toUpperCase();
        if (os.contains("WIN"))
            ConsoleApp._sys = SYS_WIN;
    }
}