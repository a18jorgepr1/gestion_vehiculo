# Aplicación: GESTIÓN VEHICULO

---

## Tabla de Contenidos

- [Aplicación: GESTIÓN VEHICULO](#aplicación-gestión-vehiculo)
  - [Tabla de Contenidos](#tabla-de-contenidos)
    - [Version](#version)
    - [OBJETIVO](#objetivo)
      - [La funcionalidad será la siguiente](#la-funcionalidad-será-la-siguiente)
      - [Deberás crear dos paquetes](#deberás-crear-dos-paquetes)
      - [La clase  Vehiculo dispondrád de los siguientes métodos](#la-clase--vehiculo-dispondrád-de-los-siguientes-métodos)
    - [OBSERVACIONES](#observaciones)
    - [REFERENCIAS](#referencias)

---

### Version

- 0.0.1-SNAPSHOT

---

### OBJETIVO

 Se trata de desarrollar una aplicación Java que permita gestionar la
información de un vehículo. Mediante un menú que aparecerá en pantalla se
podrán realizar determinadas operaciones:

1. **Nuevo vehículo.**
2. **Ver matrícula.**
3. **Ver número de kilómetros.**
4. **Actualizar kilómetros.**
5. **Ver años de antigüedad.**
6. **Mostrar propietario.**
7. **Mostrar descripción.**
8. **Mostrar precio.**
9. **Salir.**

#### La funcionalidad será la siguiente

- Al iniciar la aplicación se mostrará el menú propuesto.
- Dependiendo de la opción seleccionada por el usuario:
  - **Nuevo Vehículo:** Se creará un nuevo Vehículo, si los datos introducidos por el usuario son correctos, que contendrá la siguiente información (marca, matrícula, número de kilómetros, fecha de matriculación, descripción, precio, nombre del propietario, dni del propietario). Todos los datos serán solicitados por teclado y tan solo habrá que comprobar:
    - Que la fecha de matriculación es anterior a la actual: puedes solicitar por separado día, mes y año y construir un objeto LocalDate (tienes una referencia en el apartado Consejos y recomendaciones).
    - Que el número de kilómetros es mayor que 0.
    - Que el DNI del propietario sea correcto.
    - Si no se cumple alguna de las condiciones se deberá mostrar el correspondiente mensaje de error y se mostrará de nuevo el menú principal.
  - ****Ver Matrícula:**** Mostrará la matrícula del vehículo por pantalla.
  - ****Ver Número de Kilómetros:**** Mostrará el número de kilómetros por pantalla.
  - **Actualiza Kilómetros:** Permitirá actualizar el número de kilómetros del vehículo. Habrá que tener en cuenta que solo se podrán sumar kilómetros.
  - **Ver años de antigüedad:** Mostrará por pantalla el número de años del vehículo desde que se matriculó, no la fecha de matriculación.
  - **Mostrar propietario:** Mostrará por pantalla el nombre del propietario del vehículo junto a su dni.
  - **Mostrar Descripción:** Mostrará una descripción del vehículo, incluyendo su matrícula y el número de kilómetros que tiene.
  - **Mostrar Precio:** se mostrará el precio del vehículo.
  - **Salir:** El programa finalizará.

#### Deberás crear dos paquetes

- **vehiculo:** que contendrá la clase  **Vehiculo** y la clase  **ConsoleApp**.
- **vehiculo.util:** contendrá la clase Util con los métodos estáticos encargados de realizar las validaciones.
  
#### La clase  Vehiculo dispondrád de los siguientes métodos

- **Constructor o constructores.**
- **Métodos get y set para acceder a sus propiedades.**
- **Método getEdad():** Retorna un entero con el número de años del vehículo.

### OBSERVACIONES

- En la clase  **Vehiculo** no debe solicitar datos por teclado ni escribir datos en pantalla. Esas operaciones se realizarán en la clase **ConsoleApp**.
- En la clase  **Vehiculo** no se deben hacer validaciones de datos. Los datos se validan en la clase  **ConsoleApp** y si son correctos, se instancia el objeto  **Vehiculo**.
- La aplicación solo trabajará con un vehículo, por lo tanto, solo utilizará una referencia a un objeto de tipo  **Vehiculo** en la clase **Vehiculo**. Si ya existe un vehículo y el usuario selecciona  **Nuevo Vehículo** en el menú, se perderá la información del vehículo existente y se guardará la del nuevo.
- No será necesario realizar comprobaciones de tipo en los datos solicitados por teclado.
- No se podrán mostrar datos de un vehículo si aún no se ha creado: en ese caso habrá que mostrar un mensaje por pantalla.
- Piensa en los modificadores de acceso que debes utilizar tanto en atributos como métodos de la clase.
- En la cabecera de las clases añade documentación indicando autor y descripción de la clase.
- En la cabecera de cada método añade documentación indicando la funcionalidad que implementa y el valor que devuelve.
- El código fuente Java de esta clase debería incluir comentarios en cada atributo (o en cada conjunto de atributos) y método (o en cada conjunto de métodos del mismo tipo indicando su utilidad.

### REFERENCIAS

- [Formato NIF](https://es.wikipedia.org/wiki/N%C3%BAmero_de_identificaci%C3%B3n_fiscal)
  